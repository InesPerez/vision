clc,clear all
figure
subjects=readtable('cancer.csv');
relative_path=extract_path(subjects.od_img_path);
mask_path=extract_path(subjects.mask_path);

for i=1:numel(relative_path)
    figure
    image=imread(relative_path(i));
    mask=imread(mask_path(i));
    image(~mask)=255;
    imshow(image)
end

function relative_path=extract_path(column)

origin_path=string(column);

index=table2array(cell2table(strfind(origin_path,'ddsm_tools')))-1;
relative_path=strcat('./',extractAfter(origin_path,index)) ;

end
