/*
** Author  : K.Chang
** Date    : 03/19/97
**
** This program will create a template images based on the input of images and 
**   overlay file that contains ground truth information in DDSM Database.
** The template image is an 8 bit pgm format.
**
** This program will assume that there is no need to transform the positions 
**   between marked images and unmarked images. In other words, the transformation
**   of each points between marked and unmarked images has been completed.
*/

#include <stdio.h>
#include <string.h>
#include "header.h"

#define max(A, B) ((A) > (B) ? (A) : (B))

#define TRUE  1
#define FALSE 0
/*
** Prototypes
*/
void createTemplate();
void draw_on_UnmarkedImage();
void transformTemplate();
void fill_up_Boundary();
void fillChainCode();
void swap_bubble();
void bubblesort();
void readGTFile();
void readICS();
int  initTemplateImage();
int  Chain_Fill();
int  Chain_Status();
int  Create_X_Table();
int  Find_Index();
int  Generate_Points();
void abnormalExit();
int  writePgmImage();

extern void Chop_Chain_Code();
extern void Generate_Chain_Code();

void main (argc, argv)
int   argc;
char *argv[];
{
  char GT_filename[100];
  char Output_filename[100];
  FILE *image_fp, *jpeg_fp;
  char command[255];

  if(argc < 2)
    {
      printf("\n USAGE: createTemplate ImageFile(No .LJPEG ext.) \n\n");
      printf("\n Ex.  createTemplate A_0001_1.LEFT_CC \n\n"); 
      exit(1);
    }

  sprintf(GT_filename, "%s.OVERLAY", argv[1]); 
  image_fp = fopen(GT_filename, "r");
  
  if(image_fp == NULL)
    abnormalExit("CreateTemplate.c", "main", 
		 "This file doesn't have OVERLAY file! ");
      
  sprintf(Output_filename, "%s.pgm", GT_filename);

  createTemplate(GT_filename, Output_filename);

  printf("%s has been created.\n", Output_filename);

  return;
}
/*
========================================================================
function name  : createTemplate
function input : unmarked_filename(images in DDSM)
                 gt_filename(overlays in DDSM)
                 output_filename(pgm format images-binary)
========================================================================
*/
void createTemplate(gt_filename, output_filename)
char *gt_filename, *output_filename;
{  
  ABNIMAGE Unmarked;

  /*
  ** reads the GT file.
  ** stores the chain code into the Overlay structure.
  **
  */
  readGTFile(gt_filename); 

  draw_on_UnmarkedImage(gt_filename, output_filename);  

  
  if(writePgmImage(TemplateImage.FileName, TemplateImage.Image,
                   TemplateImage.Width, TemplateImage.Height))

  return;
}
/*
==========================================================================
function name:  draw_on_UnmarkedImage
function input: local_unmarked(images in DDSM)
                output_filename(pgm file) 
==========================================================================
*/

void draw_on_UnmarkedImage(gt_filename, output_filename)
char    *gt_filename;
char    *output_filename;
{
  char ics_filename[100], Newics_filename[100];
  char view[10];
  int  i, j, str_pt = 0;
  int  imageWidth, imageHeight;

  /*
  ** changing A_0001_1.LEFT_CC.OVERLAY  to  A-0001-1.ics
  */
  for(i = 0; i < strlen(gt_filename); i++)
    {
      if(gt_filename[i] == '_')
	{
	  ics_filename[i] = '-';
	}
      else
	ics_filename[i] = gt_filename[i];
      
      if(gt_filename[i] == '.')
	{
	  ics_filename[i] = '\0';
	  break;
	}
    }

  for(j = (i + 1); j < strlen(gt_filename); j++)
    { 
      view[str_pt] = gt_filename[j];

      if(gt_filename[j] == '.')
	{
	  view[str_pt] = '\0';
	  break;
	}
      str_pt++;
    }
      
  sprintf(Newics_filename, "%s.ics", ics_filename);
 
  /*
  ** reading an ics file to get the width and height information.
  */
  readICS(Newics_filename, view, &(imageWidth), &(imageHeight));

  initTemplateImage(imageWidth, imageHeight, output_filename, &TemplateImage);
  

  /*
  ** This function will create the Template image file without being translated.
  */
  
  fill_up_Boundary(TemplateImage);

  return; 
}
void readICS(filename, view, width, height)
char *filename;
char *view;
int  *width, *height;
{
  FILE *icsfp;
  char junk[255], junk_key1[255], junk_key2[255];
  int  icsWidth, icsHeight;

  icsfp = fopen(filename, "r");

  if(icsfp == NULL)
    abnormalExit("CreateTemplate.c", "readICS", "ics file is not found !");

  fscanf(icsfp, "%s", junk);
  
  while(strcmp(junk, view) != 0)
    fscanf(icsfp, "%s", junk);
  
  fscanf(icsfp, "%s %d %s %d", junk_key1, &icsHeight, junk_key2, &icsWidth);

  (*width)  = icsWidth;
  (*height) = icsHeight;

  fclose(icsfp);

  if(icsWidth == 0 && icsHeight == 0)
    {
      printf("%s in This Case doesn't Have Overlay Information\n", view);
      printf("Failed to Create a Template Image..\n");
      exit(0);
    }

  return;
}

/*
** Callback function that calls the "Fill_Chain" in [Read_Over.c]
**
** It fills the boundary with different colors and different labels.
*/
void fill_up_Boundary()
{ 
  Window window;
  GC     gc;
  int    i;

  for(i = 0; i < ID; i++)
    { 
      fillChainCode(Overlay.OVERLAY_Mark[i]);
    }

  return;
}
void fillChainCode(ChainCode)
Chains     ChainCode;
{
  int    i, j;
  int    CounterClockwise;
  int    tempx, tempy;
  int    Loop_Ever = 0;
  int    Done = 0;

  do
    { 
      Chop_Chain_Code(&(Overlay.OVERLAY_Mark[ID]));   

      /*
      ** Filling out the interior of the chain code.
      */
      
      if(Chain_Fill(ChainCode, NOFILL) == 0)
	CounterClockwise = TRUE;
      else
	CounterClockwise = FALSE;

      if(CounterClockwise == TRUE)
	{	  
	  Loop_Ever++;
	  if(Loop_Ever > 3)
	    {
	      printf("Line was Overlapped\n");
	      break;
	    }
	  
	  for(i = 1, j = ChainCode.Total_Points; 
	      i <= ChainCode.Total_Points/2; i++, j--)
	    {
	      /*
	      ** i -->    <-- j
	      */
	      
	      tempx = ChainCode.Points[i].x;
	      ChainCode.Points[i].x = ChainCode.Points[j].x;
	      ChainCode.Points[j].x = tempx;

	      tempy = ChainCode.Points[i].y;
	      ChainCode.Points[i].y = ChainCode.Points[j].y;
	      ChainCode.Points[j].y = tempy;
	    }
	  Generate_Chain_Code(ChainCode.Output_ChainCode, 
			      ChainCode.Total_Points, ChainCode);      
	}
    }while(CounterClockwise == TRUE);

  /*
  ** Drawing the Boundary after fixing the bad codes with FILL parameter.
  **
  */

  if((Loop_Ever < 5))
    Chain_Fill(ChainCode, FILL);
  else
    {
      printf("Line has been overlapped in this image.\n");
      printf("Please, check see if OK..\n");
    }

  Loop_Ever = 0;

  return;
}

int Chain_Fill(Code, draw)
Chains     Code;
int        draw;
{
  int Action;
  int Index = 1;
  int Count = 2;
  int Draw;
  int Vertex;
  int *X_Link;
  int i, j;
  int Loop_Ever;
  int Infinite;
  int ucLabel_Value;

  /*
  ** Assigning a gray level value to each class.
  */

  ucLabel_Value = 1;
  Action = NOTNOP;
  Draw   = STOP;
  Vertex = NO;
  Infinite = FALSE;
  Loop_Ever = 0;

  for(i = 1; i <= Code.Total_Points; i++, Index++)
    {      
      if(!Create_X_Table(Code, Code.Points[i].x, Code.Points[i].y, &X_Link))
	{
	  printf("Function Create_X_Table is not completed !\n");
	  return;
	}
      
      while(Action != NOP && Infinite != TRUE)
	{
	  if(Vertex == YES && Draw == CONTINUE)
	    ;
	  else
	    {
	      if(Index == 1)
		{
		  Action = 
		    Chain_Status(Code.Output_ChainCode[Code.Total_Points],
				 Code.Output_ChainCode[Index]);
		}
	      else
		{
		  Action = 
		    Chain_Status(Code.Output_ChainCode[Index - 1],
				 Code.Output_ChainCode[Index]); 
		}
	    }
	  switch(Action)
	    {
	    case START_FILLING:
	      {
		Draw = CONTINUE;
		if(draw == FILL)
		  {
		    for(j = Code.Points[Index].x; j <= X_Link[Count]; j++)
		      {
			TemplateImage.Image[Code.Points[Index].y * 
					   TemplateImage.Width + j]
			  = (unsigned char)ucLabel_Value;
		      }
		  }

		Index = Find_Index(X_Link[Count], Code.Points[i].y, Code);

		if(Count <= X_Link[0])
		  Count++;

		Vertex = NO;
		Loop_Ever++;


		if(Loop_Ever > Code.Total_Points)
		  {
		    Infinite = TRUE;		    
		    break;
		  }

		break;
	      }
	    case TANGENT:
	      {
		if(draw == FILL)
		  {
		    for(j = Code.Points[Index].x; j <= X_Link[Count]; j++)
		      {
			TemplateImage.Image[Code.Points[Index].y * 
					   TemplateImage.Width + j]
			  = (unsigned char)ucLabel_Value;
		      }
		  }

		Action = NOP;
		Draw   = STOP;
		break;
	      }
	    case STOP_FILLING:
	      {
		Action = NOP;
		Draw   = STOP;
		break;
	      }	      
	    case VERTEX:
	      {
		if(Draw == CONTINUE)
		  {
		    Action = START_FILLING;
		    Vertex = YES;
		  }
		else
		  Action = NOP;
		break;
	      }
	    default:
	      {
		Action = NOP;
		break;
	      }
	    }/* End of switch */

	  if(X_Link != NULL)
	    free(X_Link);

	}/* End while loop */

      Action = NOTNOP;
      Count = 2;
      Index = i;

      if(Infinite == TRUE)
	break;

    }/* End of for loop */

  for(i = 1; i <= Code.Total_Points; i++, Index++)
    {      
      TemplateImage.Image[Code.Points[i].y * TemplateImage.Width + 
			 Code.Points[i].x]
	= (unsigned char)ucLabel_Value;
    }

  if(Infinite == TRUE)
    return 0;
  else
    return 1;
}

int Chain_Status(PR_Code, P_Code)
int PR_Code;
int P_Code;
{
  int Action;

  switch(P_Code)
    {
    case 0:
      {
	if((PR_Code == 3) || (PR_Code == 5))
	  Action = VERTEX;
	
	else if((PR_Code != 3) || (PR_Code != 5))
	  Action = START_FILLING;
	
	else
	  Action = NOP;
	break;
      }
    case 1:
      {
	if((PR_Code == 3) || (PR_Code == 4))
	  Action = VERTEX;
	
	else if((PR_Code != 3) || (PR_Code != 4))
	  Action = START_FILLING;
	
	else
	  Action = NOP;
	break;
      } 
    case 2: 
      {
	Action = TANGENT;
	break;
      }
    case 3: 
      {
	if((PR_Code == 0) || (PR_Code == 1))
	  Action = VERTEX;
	    
	else if((PR_Code != 0) || (PR_Code != 1))
	  Action = STOP_FILLING;
	
	else
	  Action = NOP;
	break;
      }
    case 4: 
      {
	if((PR_Code == 1) || (PR_Code == 7))
	  Action = VERTEX;
	
	else if((PR_Code != 1) || (PR_Code != 7))
	  Action = STOP_FILLING;
	
	else
	  Action = NOP;
	break;
      }
    case 5: 
      {
	if((PR_Code == 0) || (PR_Code == 7))
	  Action = VERTEX;
	
	else if((PR_Code != 0) || (PR_Code != 7))
	  Action = STOP_FILLING;
	
	else
	  Action = NOP;
	break;
      }
    case 6:
      {
	if((PR_Code == 3) || (PR_Code == 4) || (PR_Code == 5))
	  Action = STOP_FILLING;
	
	else if((PR_Code != 3) || (PR_Code != 4) || (PR_Code != 5))
	  Action = TANGENT;
	
	else
	  Action = NOP;
	break;
      } 
    case 7: 
      {
	if((PR_Code == 4) || (PR_Code == 5))
	  Action = VERTEX;
	
	else if((PR_Code != 4) || (PR_Code != 5))
	  Action = START_FILLING;
	
	else
	  Action = NOP;
	break;
      }
    default:
      break;
    }

  return(Action);
}

void swap_bubble(values, left, right)
     int values[];
     int left;
     int right;
{
   int temp;

   temp = values[left];
   values[left] = values[right];
   values[right] = temp;
}
/* 
** bubblesort a sequence of integers 
*/     

void bubblesort(values, left_bound, right_bound)
     int values[];
     int left_bound;
     int right_bound;
{
  int left;
  int right;
  
  for (left = left_bound; left < right_bound; left++) 
    {
      for (right = right_bound; right > left; right--) 
	{
	  if (values[right] <= values[right - 1])
	    swap_bubble(values, right, right - 1);
	}
    }
  return;
}


int Create_X_Table(Code, x, y, X_Link)
     Chains Code;
     int    x;
     int    y;
     int **X_Link;
{
  int count = 1;
  int i;
 
  for(i = 1; i <= Code.Total_Points; i++)
    {
      if(Code.Points[i].y == y && Code.Points[i].x >= x)
	count++;
    }
  
  (*X_Link) = (int *)malloc(count * sizeof(int));
      
  if((*X_Link) == NULL)
    {
      printf("ERROR: Memory Allocation Problem !\n");
      return(0);
    }

  (*X_Link)[0] = count - 1;

  if((*X_Link)[0] < 1)
    return;

  count = 1;

  for(i = 1; i <= Code.Total_Points; i++)
    {
      if(Code.Points[i].y == y && Code.Points[i].x >= x)
	{
	  (*X_Link)[count] = Code.Points[i].x;
	  count++;
	}
    }

  bubblesort((*X_Link), 1, (*X_Link)[0]); 

  return(1);
}
int Find_Index(x, y, Code)
     int    x;
     int    y;
     Chains Code;
{
 int i;
 
  for(i = 1; i <= Code.Total_Points; i++)
    {
      if((x == Code.Points[i].x) && (y == Code.Points[i].y))
	break;
    }
 
  return(i);
}

void readGTFile(FileName)
char *FileName;
{
  FILE *file_ptr;
  char words[30], Junk[30];
  int i = 0, j, count = 0;
  int *temp_chain;
  int IsCore = NO;

  temp_chain = (int *)malloc(10000 * sizeof(int));

  if(temp_chain == NULL)
    {
      printf("ERROR: Out of memory!\n");
      return;
    }

  file_ptr = fopen(FileName, "r");
  if(file_ptr == NULL)
    {
      printf("ERROR: Overlay File Not Found !\n");
      return;
    }
  fscanf(file_ptr, "%s", words);
  fscanf(file_ptr, "%d", &(Overlay.Total_Abnormality));

  while(1)
    {
      while(fscanf(file_ptr, "%s", words) == 1)
        {
          if(!strcmp(words, "#"))/* end of chain code */
	    break;
	  
	  if(!strcmp(words, "CORE")) /* skip those infos */
	    {
	      IsCore = YES;
	      i = 18;
	    }
          else if(i == 0)
            {
              strcpy(Junk, words);
            }
          else if(i == 1)
            {
	      Overlay.OVERLAY_Mark[ID].Abnormality = (int)atoi(words);
            }
	  else if(i == 2)
            {
	      strcpy(Junk, words);
            }
          else if(i == 3)
            {
              strcpy(Overlay.OVERLAY_Mark[ID].Lesion_Type, words);
            }
          else if(i == 4)
            {/*Shape or Type*/
              strcpy(Overlay.OVERLAY_Mark[ID].Lesion_Keywd1, words);
            }
          else if(i == 5)
            {
	      strcpy(Overlay.OVERLAY_Mark[ID].Lesion_Subwd1, words);
            }  
	  else if(i == 6)
            {/*Margins or Distributions*/
	      strcpy(Overlay.OVERLAY_Mark[ID].Lesion_Keywd2, words);
            }
          else if(i == 7)
            {
              strcpy(Overlay.OVERLAY_Mark[ID].Lesion_Subwd2, words);
            }
          else if(i == 8)
            {
              strcpy(Junk, words);
            }
	  else if(i == 9)
            {
	      Overlay.OVERLAY_Mark[ID].Assessment = (int)atoi(words);
            }  
	  else if(i == 10)
            {
	      strcpy(Junk, words);
            }
          else if(i == 11)
            {
              Overlay.OVERLAY_Mark[ID].Subtlety = (int)atoi(words);
            }  
	  else if(i == 12)
            {
              strcpy(Junk, words);
            }
          else if(i == 13)
            {
              Overlay.OVERLAY_Mark[ID].M_B = (int)atoi(words);
            }
          else if(i == 14)
            {
              strcpy(Junk, words);
            }
          else if(i == 15)
            {
	      strcpy(Overlay.OVERLAY_Mark[ID].Pathology, words);
            } 
	  else if(i == 16)
            {
              strcpy(Junk, words);
            }
          else if(i == 17)
            {
	      Overlay.OVERLAY_Mark[ID].Total_Outline = (int)atoi(words);
            } 
	  else if(i == 18)
            {
	      NULL; /*boundary or core*/
            }
          else if(i == 19)
            {
              Overlay.OVERLAY_Mark[ID].Points[1].x = (int)atoi(words);
            }
          else if(i == 20)
            {
              Overlay.OVERLAY_Mark[ID].Points[1].y = (int)atoi(words);
            } 	 
          else if(i >= 21)
            {
	      temp_chain[i - 20] = (int)atoi(words);	
	      count++;
            }
          else
            NULL;
	  
          i++;
        }/* end while(fscanf) */
      
      if(i == 0 || count == 0)
      break;

      /*
      ** After Reading a set of chain codes
      */
      
      Overlay.OVERLAY_Mark[ID].Total_Points = count;
      
      Overlay.OVERLAY_Mark[ID].Output_ChainCode = (int*) 
	calloc(Overlay.OVERLAY_Mark[ID].Total_Points + 10, sizeof(int));
      
      if(Overlay.OVERLAY_Mark[ID].Output_ChainCode == NULL)
	printf("ERROR: Memory Problem in Reading Overlay file!\n");
      
      for(j = 1; j <= Overlay.OVERLAY_Mark[ID].Total_Points; j++)
	Overlay.OVERLAY_Mark[ID].Output_ChainCode[j] = temp_chain[j];
      
      
      /*
      ** After reading the overlay once, check how many class do I have
      ** in this session.
      */

      if(ID > 0)
	if(Overlay.OVERLAY_Mark[ID].Class!=Overlay.OVERLAY_Mark[ID - 1].Class)
	  Num_ClassG++;

      /*
      ** Generate x, y points with a given chain codes
      */	  
      Generate_Points(&(Overlay.OVERLAY_Mark[ID])); 
      
      i     = 0;
      count = 0;
      
      ID++;
      
    }/* end while() */
  
  fflush(file_ptr);
  fclose(file_ptr);
  free(temp_chain);
  return;
}

/*
** Initialize the the Global var. TemplateImage(ABNIMAGE) sturcture.
*/
int initTemplateImage(width, height, filename, Template)
int      width, height;
char     *filename;
ABNIMAGE *Template;
{
  /*
  Widget             widget;
  unsigned char      *Image;
  unsigned short int *Image_16;
  int                Width;
  int                Height;
  int                Width_16;
  int                Height_16;
  int                Image_Position_LTop_V;
  int                Image_Position_LTop_H; 
  char               FileName[200];
  */

  int i;

  strcpy(Template->FileName, filename);

  Template->Width  = width;
  Template->Height = height;

  Template->Image = (unsigned char *)malloc(Template->Width 
					    * Template->Height 
					    * sizeof(unsigned char));
  
  if(Template->Image == NULL)
    {
      printf("ERROR: memory problem !! in createTmplt.c\n");
      return 1; /* abnormal process */
    }

  /*
  ** Initializing the array.
  */
  for(i = 0; i < Template->Width * Template->Height; i++)
    Template->Image[i] = (unsigned char)0;

  return 0; /* normal process */
}

void Get_Title(target, source)
char target[], source[];
{
  int  i = 0, j, k;

  j = strlen(source);
  
  while(i < j)
    {
      if(*(source + i) == '/')
	k = i;
      i++;
    }
  strcpy(target, "");
  strcat(target, (source + k + 1));
}

int Generate_Points(Code)
Chains *Code;
{

  int    i;

  for(i = 1; i <= Code->Total_Points; i++)
    {
     
      switch(Code->Output_ChainCode[i])
	{
	case UP:
	  {
	    Code->Points[i + 1].x = Code->Points[i].x;
	    Code->Points[i + 1].y = Code->Points[i].y - 1;
	    break;
	  }
	      
	case RIGHT_UP: 
	  {
	    Code->Points[i + 1].x = Code->Points[i].x + 1;
	    Code->Points[i + 1].y = Code->Points[i].y - 1;
	    break;
	  }
	  
	case RIGHT:
	  {
	    Code->Points[i + 1].x = Code->Points[i].x + 1;
	    Code->Points[i + 1].y = Code->Points[i].y;
	    break;
	  }
	      
	case RIGHT_DOWN:
	  {
	    Code->Points[i + 1].x = Code->Points[i].x + 1;
	    Code->Points[i + 1].y = Code->Points[i].y + 1;
	    break;
	  }
	      
	case DOWN: 
	  {
	    Code->Points[i + 1].x = Code->Points[i].x;
	    Code->Points[i + 1].y = Code->Points[i].y + 1;
	    break;
	  }
	      
	case LEFT_DOWN:
	  {
	    Code->Points[i + 1].x = Code->Points[i].x - 1;
	    Code->Points[i + 1].y = Code->Points[i].y + 1;
	    break;
	  }
	      
	case LEFT:
	  {
	    Code->Points[i + 1].x = Code->Points[i].x - 1;
	    Code->Points[i + 1].y = Code->Points[i].y;
	    break;
	  }
	      
	case LEFT_UP:
	  {
	    Code->Points[i + 1].x = Code->Points[i].x - 1;
	    Code->Points[i + 1].y = Code->Points[i].y - 1;
	    break;
	  }
	default:
	  {
	    printf("Wrong Operation !! %d at %d\n",
		   Code->Output_ChainCode[i], i);
	    break;
	  }
	}
    }

  return(1);
}

int writePgmImage(file_name, image_array, image_width, image_height)

char            file_name[];
unsigned char   *image_array;
int             image_width, image_height;
{
  FILE    *output_file, *fopen();
  char    dummy[255];
  int     i,j;

  output_file = fopen(file_name, "w");
  if(output_file == NULL)
  {
    printf("\nERROR: cannot open file %s\n", file_name);
    return(1);
  }
  
  fprintf(output_file, "%s\n" , "P5");
  fprintf(output_file, "%d %d\n", image_width, image_height);
  fprintf(output_file, "%d\n", 255);

  fwrite(image_array, 1, image_width*image_height, output_file);
  fflush(output_file);
  fclose(output_file);
  return(0);
}

void abnormalExit(file_name, function_name, error_message)
char *function_name;
char *file_name;
char *error_message;

{
  printf("Warning: Invalid operation in FILE: %s\n", file_name);
  printf("                       in Function: %s\n", function_name);
  printf("Message: %s\n", error_message);

  exit(-99);
}
