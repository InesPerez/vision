#include "header.h"

int Generate_Chain_Code(Out_Chain_Code, Tot_Points, Code)
int *Out_Chain_Code;
int Tot_Points;
Chains Code;
{

  int    Current_X, Current_Y, Previous_X, Previous_Y, The_Direction;
  int    Index = 1, i, final_close = 0;

  for(i = 1; i <= Tot_Points; i++)
    {
      Previous_X = Code.Points[Index].x;
      Previous_Y = Code.Points[Index].y;

      if(final_close == 0)
	{	
	  Current_X = Code.Points[Index + 1].x; 
	  Current_Y = Code.Points[Index + 1].y;
	}
      else
	{  
	  Current_X = Code.Points[1].x; 
	  Current_Y = Code.Points[1].y;
	}

      if(Current_X == Previous_X)
	{
	  if(Current_Y == (Previous_Y - 1))
	    The_Direction = UP;
	  
	  if(Current_Y == (Previous_Y + 1))
	    The_Direction = DOWN;
	}
      else if(Current_Y == Previous_Y)
	{
	  if(Current_X == (Previous_X - 1))
	    The_Direction = LEFT;
	  
	  if(Current_X == (Previous_X + 1))
	    The_Direction = RIGHT;
	}
      else if(Current_X > Previous_X && Current_Y > Previous_Y)
	{
	  The_Direction = RIGHT_DOWN;
	}  
      else if(Current_X > Previous_X && Current_Y < Previous_Y)
	{
	  The_Direction = RIGHT_UP;
	}
      else if(Current_X < Previous_X && Current_Y > Previous_Y)
	{
	  The_Direction = LEFT_DOWN;
	}
      else if(Current_X < Previous_X && Current_Y < Previous_Y)
	{
	  The_Direction = LEFT_UP;
	}
      else
	printf("ERROR: in Chain.c\n");

      Out_Chain_Code[Index] = The_Direction; 
      

      if(Index == (Tot_Points - 1))
	final_close = 1;

      Index ++;
      
    }
  return(1);

}
void Chop_Chain_Code(Code)
Chains *Code;
{
  Chain_Code *Temp_Chain;
  int next_x, next_y, init_x, init_y, Print, diff_X, diff_Y;
  long Total_Num = 1, How_Many_Gap = 0;
  long i, j, x, y;
    
  Temp_Chain = (Chain_Code *)malloc(Code->Total_Points * 3 * 
				    sizeof(Chain_Code));

  Print = False;

  /*
  ** Checking Repeatition in Chain_Code
  */

  init_x = Code->Points[1].x;
  init_y = Code->Points[1].y;

  for(i = 2; i <= Code->Total_Points; i++)
    {
      next_x = Code->Points[i].x;
      next_y = Code->Points[i].y;

      if(init_x == next_x && init_y == next_y)
	{
	  for(j = i - 1; j < Code->Total_Points; j++)
	    {
	      Code->Points[j].x = Code->Points[j + 1].x;
	      Code->Points[j].y = Code->Points[j + 1].y;
	    }

	  /*
	  ** -999 indicates the end of chain code.
	  */

	  Code->Points[Code->Total_Points].x = -999;
	  Code->Points[Code->Total_Points].y = -999;

	  Code->Total_Points--;

	  i--;
	}

      init_x = Code->Points[i].x;
      init_y = Code->Points[i].y;
    }

  /*
  ** Checking Pass-Overs in Chain_Code
  ** Finding Empty Holes and Filling.
  */
  init_x = Code->Points[1].x;
  init_y = Code->Points[1].y;

  Temp_Chain[Total_Num].x = Code->Points[1].x;
  Temp_Chain[Total_Num].y = Code->Points[1].y;

  for(i = 2; i <= Code->Total_Points + 2; i++)
    {
      if(i > Code->Total_Points)
	i = 1;

      next_x = Code->Points[i].x;
      next_y = Code->Points[i].y;


      Total_Num++;
      Temp_Chain[Total_Num].x = Code->Points[i].x;
      Temp_Chain[Total_Num].y = Code->Points[i].y;

      if((abs(next_x - init_x) != 1 && abs(next_x - init_x) != 0) || 
	 ((abs(next_y - init_y) != 1 && abs(next_y - init_y) != 0)))
	{
	  diff_X = abs(next_x - init_x);
	  diff_Y = abs(next_y - init_y);

	  if((next_x > init_x) && (diff_X >= diff_Y))
	    for(x = init_x + 1; x < next_x; x++)
	      {         
		y = ((x - next_x)*(init_y - next_y))/(init_x - next_x)
		  + next_y;
		
		Temp_Chain[Total_Num].x = x;
		Temp_Chain[Total_Num].y = y;
		
		Total_Num++;
		How_Many_Gap++;
	      }
	  else if((next_x < init_x) && (diff_X >= diff_Y))
	    for(x = init_x - 1; x > next_x; x--)
	      {         
		y = ((x - next_x)*(init_y - next_y))/(init_x - next_x)
		  + next_y;
		
		Temp_Chain[Total_Num].x = x;
		Temp_Chain[Total_Num].y = y;
		Total_Num++;
		How_Many_Gap++;
	      }

	  else if((next_y > init_y) && (diff_Y >= diff_X))
	    for(y = init_y + 1; y < next_y; y++)
	      {         
		x = ((y - next_y)*(init_x - next_x))/(init_y - next_y)
		  + next_x;
		
		Temp_Chain[Total_Num].x = x;
		Temp_Chain[Total_Num].y = y;
		Total_Num++;
		How_Many_Gap++;
	      }
	  else if((next_y < init_y) && (diff_Y >= diff_X))
	    for(y = init_y - 1; y > next_y; y--)
	      {         
		x = ((y - next_y)*(init_x - next_x))/(init_y - next_y)
		  + next_x;
		
		Temp_Chain[Total_Num].x = x;
		Temp_Chain[Total_Num].y = y;
		Total_Num++;
		How_Many_Gap++;
	      }
	  
	  Temp_Chain[Total_Num].x = Code->Points[i].x;
	  Temp_Chain[Total_Num].y = Code->Points[i].y;
	}
      else
	;

      if(i == 1)
	break;

      init_x = Code->Points[i].x;
      init_y = Code->Points[i].y;

    }
  
  /*
  ** Now, store back to Code->Points after checking
  **  Repeatitions and Pass-overs.
  */

  if(Total_Num > Code->Total_Points)
    {
      for(i = 1; i <= Total_Num; i++)
	{
	  Code->Points[i].x = Temp_Chain[i].x;
	  Code->Points[i].y = Temp_Chain[i].y;
	}
    }
      
  Code->Total_Points = Code->Total_Points + How_Many_Gap;
	  
  free(Temp_Chain);

  return;
}




