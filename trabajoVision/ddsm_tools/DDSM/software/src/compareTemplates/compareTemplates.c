#include <stdio.h>
#include <math.h>

#define	BACKGROUND	0

/************************************************************************/
/* Types of performance metrics for detection algorithms.		*/
#define OVERLAP		0 /* detection must overlap truth region by	*/
			  /* some minimum amount.			*/
#define LOCATION	1 /* centroid of detection should lie within	*/
			  /* the truth region (i.e., points to a 	*/
			  /* suspicious region.				*/

/* A template (i.e. ground truth) image - an overlay for a raw image.	*/
typedef struct
{
  unsigned char      **Image;
  int                Rows;
  int                Cols;
} TEMPLATE_IMAGE;


/************************************************************************/
/* Load a mammogram image in DBA Scanner format, segment the breast 	*/
/* tissue from the background, and segment breast tissue into fatty and	*/
/* dense regions.							*/
/************************************************************************/
main(argc, argv)
int  argc;
char *argv[];
{
TEMPLATE_IMAGE	*Load_PgmTemplateImage();
TEMPLATE_IMAGE	*AllocateTemplate();
TEMPLATE_IMAGE	*Scale_Template();
void		Compare_TemplateImages();
void 		FreeTemplate();

char		Truth_filename[256], Detection_filename[256], ResFilename[256];
TEMPLATE_IMAGE	*Truth, *Detection;
TEMPLATE_IMAGE	*ScaledTruth, *ScaledDetection;
float		GTResolution, DResolution, ProcessResolution, OvThresh;
int		MetricType;

if (argc < 7){
  printf("Compare Templates Usage:\n");
  printf("Compare-Templates GT_Filename GT_SpatialRes D_Filename D_SpatialRes P_SpatialRes Metric [Thresh] Outfile\n\n");
  printf("GT_Filename   = filename of ground truth template (PGM image format)\n");
  printf("GT_SpatialRes = spatial resolution of ground truth in microns per pixel\n");
  printf("D_Filename    = filename of detection template (PGM image format)\n");
  printf("D_SpatialRes  = spatial resolution of detection template in microns per pixel\n");
  printf("P_SpatialRes  = spatial resolution for comparison of templates\n");
  printf("Metric        = 0 for OVERLAP, 1 for LOCATION\n");
  printf("Thresh	= threshold for region overlap to be considered a TP (0.0 to 1.0)\n");
  printf("Outfile       = name of output file for accumulating results\n");
  exit(99);
}


/************************************************************************/
/* Command line arguments.						*/
/* Name of ground truth image file.	*/
sprintf(Truth_filename,"%s",argv[1]);
GTResolution = atof(argv[2]);

/* Name of detection image file.	*/
sprintf(Detection_filename,"%s",argv[3]);
DResolution = atof(argv[4]);

/* Must be coarser than GTResolution and DResolution. */
ProcessResolution = atof(argv[5]);

/* Either LOCATION or OVERLAP. */
MetricType = atoi(argv[6]);

OvThresh = 0.5;
if (argc == 8){
  sprintf(ResFilename,"%s",argv[7]);
}else if (argc == 9){
  OvThresh = atof(argv[7]);
  sprintf(ResFilename,"%s",argv[8]);
}else{
  printf("ERROR in command line\n");
  exit(99);
}

/************************************************************************/
/* Load the truth image. Assume PGM image format. 			*/
Truth = Load_PgmTemplateImage(Truth_filename);
printf("Truth Template Loaded: %d rows by %d columns\n",Truth->Rows,Truth->Cols);

/************************************************************************/
/* Scale the truth template to appropriate spatial resolution.		*/
ScaledTruth = Scale_Template(Truth, GTResolution, ProcessResolution);
printf("Truth Template scaled from %5.1f to %5.1f microns per pixel\n",GTResolution,ProcessResolution);
FreeTemplate(Truth);

/************************************************************************/
/* Load the detection image. Assume PGM image format. 			*/
Detection = Load_PgmTemplateImage(Detection_filename);
printf("Detection Template Loaded: %d rows by %d columns\n",Detection->Rows,Detection->Cols);

/************************************************************************/
/* Scale the detection template to appropriate spatial resolution.	*/
ScaledDetection = Scale_Template(Detection, DResolution, ProcessResolution);
printf("Detection Template scaled from %5.1f to %5.1f microns per pixel\n",DResolution,ProcessResolution);
if ( (ScaledTruth->Rows != ScaledDetection->Rows) || 
     (ScaledTruth->Cols != ScaledDetection->Cols) ){
  printf("ERROR: Detection template dimensions do not match truth template dimensions\n");
  exit(99);
}
FreeTemplate(Detection);

Compare_TemplateImages(ScaledTruth, ScaledDetection, MetricType, OvThresh, ResFilename);

FreeTemplate(ScaledDetection);
FreeTemplate(ScaledTruth);

}/*main*/

/************************************************************************/
/* Allocates a float vector from nl to nh.				*/
/************************************************************************/
float 	*vector(nl,nh)
int 	nl, nh;
{
float 	*v;

v = (float *) calloc( (nh-nl+1),sizeof(float));
if(!v){
  printf("Allocation falure in vector()\n");
  exit(99);
}
return(v-nl);

}/*vector*/

/************************************************************************/
/* Allocates an integer vector from nl to nh.				*/
/************************************************************************/
int 	*ivector(nl,nh)
int 	nl, nh;
{
int 	*v;

v = (int *) calloc( (nh-nl+1),sizeof(int));
if(!v){
  printf("Allocation falure in ivector()\n");
  exit(99);
}
return(v-nl);

}/*ivector*/

/************************************************************************/
/* Allocates an integer matrix of size [nrl to nrh][ncl to nch].	*/
/************************************************************************/
int 	**imatrix(nrl,nrh,ncl,nch)
int 	nrl, nrh, ncl, nch;
{
register i;
int 	**m;

/* Allocate pointers to rows */
m = (int **) calloc( (nrh-nrl+1),sizeof(int*));
if(!m){
  printf("Allocation falure in row alloc. in imatrix()\n");
  exit(99);
}
m -= nrl;

/* Allocate rows and set pointers to them */
for(i=nrl;i<=nrh;i++){
  m[i]=(int *) calloc(  (nch-ncl+1),sizeof(int));
  if(!m[i]){
    printf("Allocation falure 2  in imatrix()\n");
    exit(99);
  }
  m[i] -= ncl;
}

/* return pointer to array of pointers to rows */
return m;

}/*imatrix*/

/************************************************************************/
/* Allocates a byte matrix of size [nrl to nrh][ncl to nch].		*/
/************************************************************************/
unsigned char 	**bmatrix(nrl,nrh,ncl,nch)
int 	nrl, nrh, ncl, nch;
{
register i;
unsigned char **m;

/* Allocate pointers to rows */

m = (unsigned char **) calloc( (nrh-nrl+1),sizeof(unsigned char*));
if(!m){
  printf("Allocation falure in row alloc. in bmatrix()\n");
  exit(99);
}
m -= nrl;

/* Allocate rows and set pointers to them */
for(i=nrl;i<=nrh;i++){
  m[i]=(unsigned char *) calloc(  (nch-ncl+1),sizeof(unsigned char));
  if(!m[i]){
    printf("Allocation falure 2  in bmatrix()\n");
    exit(99);
  }
  m[i] -= ncl;
}

/* return pointer to array of pointers to rows */
return m;

}/*bmatrix*/

/************************************************************************/
/* Frees a float vector allocated by vector().				*/
/************************************************************************/
void 	free_vector(v,nl,nh)
float 	*v;
int 	nl, nh;
{
free((char*) (v+nl));

}/*free_vector*/

/************************************************************************/
/* Frees a integer vector allocated by ivector().			*/
/************************************************************************/
void 	free_ivector(v,nl,nh)
int 	*v, nl, nh;
{
free((char*) (v+nl));

}/*free_ivector*/

/************************************************************************/
/* Frees a matrix allocated with imatrix().				*/
/************************************************************************/
void 	free_imatrix(m,nrl,nrh,ncl,nch)
int 	**m;
int 	nrl, nrh, ncl, nch;
{
register i;

for(i=nrh;i>=nrl;i--) free((char*) (m[i]+ncl));
free((char*) (m+nrl));

}/*free_imatrix*/

/************************************************************************/
/* Allocate space for a template. Image dimensions are from 1 to Rows by*/
/* 1 to Cols. The image array is initialized to all zeroes via calloc.	*/
/************************************************************************/
TEMPLATE_IMAGE	*AllocateTemplate(Rows, Cols)
int		Rows, Cols;
{
TEMPLATE_IMAGE	*ImTemplate;
register 	i;

ImTemplate        = (TEMPLATE_IMAGE *)malloc(sizeof(TEMPLATE_IMAGE));
ImTemplate->Rows  = Rows;
ImTemplate->Cols  = Cols;
ImTemplate->Image = bmatrix(1,Rows,1,Cols);
return(ImTemplate);

}/*AllocateTemplate*/

/************************************************************************/
/* Free space allocated to a TEMPLATE image.				*/
/************************************************************************/
void 		FreeTemplate(ImTemplate)
TEMPLATE_IMAGE	*ImTemplate;
{
register i;

for(i=ImTemplate->Rows; i>=1; i--) free( (char*)(ImTemplate->Image[i]+1) );
free( (char*)(ImTemplate->Image+1) );
free( (char*)(ImTemplate) );

}/*FreeTemplate*/

/************************************************************************/
/* Load a template image that was stored in PGM format.			*/
/************************************************************************/
TEMPLATE_IMAGE	*Load_PgmTemplateImage(fname)
char		fname[];
{
FILE    	*infile, *fopen();
register	i;
char		image_fname[256], com1[256], com2[256], junk[255];
int		Rows = 0, Cols = 0;
TEMPLATE_IMAGE	*Im;
int		Compressed;

char            line[255];

Compressed = 0;
if ((infile = fopen(fname,"r")) == NULL){
  /* See if the image is compressed */
  sprintf(image_fname,"%s.gz",fname);
  if ((infile = fopen(image_fname,"r")) == NULL){
    printf("\nERROR: PGM image file %s not found\n", fname);
    exit(99);
  }else{
    fclose(infile);
    Compressed = 1;
    sprintf(com1,"gunzip %s.gz",fname);
    sprintf(com2,"gzip %s",fname);
    system(com1);
    if ((infile = fopen(fname,"r")) == NULL){
      printf("\nERROR opening PGM image file %s\n", fname);
      exit(99);
    }
  }
}

/*
fscanf(infile,"%s",junk);
fscanf(infile,"%d %d",&Cols, &Rows);
fscanf(infile,"%s",junk);
*/

  fgets(line, 255, infile);
  if(strncmp(line, "P5", 2) != 0)
  {
    printf("\nERROR: file %s is not in pgm format\n", fname);
    exit(1);
  }
  while(1)
  {
    fgets(line, 255, infile);
    if(line[0] != '#')
    {
      Cols = atoi(strtok(line," "));
      Rows = atoi(strtok(NULL," "));
      fgets(line, 255, infile);
      break;
    }
  }

Im = AllocateTemplate(Rows, Cols);

for (i=1; i<=Rows; i++)
  fread((Im->Image[i]), 1, Cols, infile);

fflush(infile);
fclose(infile);

if (Compressed) system(com2);

return(Im);

}/*Load_PgmTemplateImage*/

/************************************************************************/
/* Compare a ground truth mask with a detection template. The truth	*/
/* mask and detection template should be 1 for abnormal pixels, and 0 	*/
/* for background pixels.						*/
/************************************************************************/
void		Compare_TemplateImages(Truth, Detection, MetricType, OvThresh, ResFilename)
TEMPLATE_IMAGE	*Truth;
TEMPLATE_IMAGE	*Detection;
int		MetricType;
float		OvThresh;
char		ResFilename[];
{
FILE    	*outfile, *fopen();
int		i, j, r, c, t, d, Rows, Cols;
int		NTargets, NDetections, curr, curc;
int		QPtr, QSize, **PixelQ, RegionSize;
int		*TargetSize, *DetectionSize, *TargetStatus, *DetectionStatus;
int		Intersection, Union;
int		TPPixelCount, FPPixelCount, TPs, FPs;
int		AbnArea, NrmlArea, **PixelOverlap, **Centroid;
float		TPRate, FPRate, OverlapMeasure, *RTot, *CTot;

printf("-----------------------------------------\n");

Rows = Truth->Rows;
Cols = Truth->Cols;

if (Detection->Rows == Rows && Detection->Cols == Cols){

  /* Count the number of abnormal regions using 4-connected region growing */
  NTargets = 0;
  QSize    = 100000;
  PixelQ   = imatrix(1,QSize,1,2);
  for (i=1; i<=Rows; i++){
    for (j=1; j<=Cols; j++){
      if (Truth->Image[i][j] == 1){

        NTargets++;
        /* Region growing set-up.*/
        QPtr            = 1;
        PixelQ[QPtr][1] = i;
        PixelQ[QPtr][2] = j;
        RegionSize      = 0;
        while ( QPtr>=1 && QPtr<=(QSize-5) ){

	  /* Grab the next pixel in the queue and process it.*/
          curr = PixelQ[QPtr][1];
          curc = PixelQ[QPtr][2];
          QPtr--;
          RegionSize++;
          Truth->Image[curr][curc] = NTargets+1;

  	  /* Add 4-connected neighbors to the queue.*/
          for (r=(curr-1); r<=(curr+1); r++){
            for (c=(curc-1); c<=(curc+1); c++){
              if (r==curr || c==curc){
                if (r>=1 && r<=Rows && c>=1 && c<=Cols){
                  if (Truth->Image[r][c] == 1){
                    QPtr++;
                    PixelQ[QPtr][1] = r;
                    PixelQ[QPtr][2] = c; 
    	            Truth->Image[r][c] = NTargets+1;
                  }            
                }
              }
            }
          }
        }

        if ( QPtr > (QSize-5) ){
          printf("WARNING - Queue for Region Growing Too Small.\n");
          printf("          A Target Object Has Been Split.\n");
        }

      }
    }
  }
  free_imatrix(PixelQ,1,QSize,1,2);
  printf("%d Target(s) present\n",NTargets);

  TPPixelCount = 0;
  FPPixelCount = 0;
  AbnArea      = 0;
  for (i=1; i<=Rows; i++){
    for (j=1; j<=Cols; j++){

      if (Truth->Image[i][j] >= 1) AbnArea++;

      if (Truth->Image[i][j] >= 1 && 
          Detection->Image[i][j] == 1) TPPixelCount++;
      if (Truth->Image[i][j] == 0 && 
          Detection->Image[i][j] == 1) FPPixelCount++;

    }
  }
  printf("Pixel Level Results...\n");
  printf("%d Abnormal pixels\n",AbnArea);

  TPRate = (float)TPPixelCount / (float)AbnArea * 100.0;
  printf("TP rate = %6.2f, or %d pixels\n",TPRate,AbnArea);
  printf("FP area = %d pixels\n\n",FPPixelCount);

  /* Count the number of detected regions using 4-connected region growing */
  NDetections = 0;
  PixelQ      = imatrix(1,QSize,1,2);
  for (i=1; i<=Rows; i++){
    for (j=1; j<=Cols; j++){
      if (Detection->Image[i][j] == 1){

        NDetections++;

        /* Region growing set-up.*/
        QPtr            = 1;
        PixelQ[QPtr][1] = i;
        PixelQ[QPtr][2] = j;
        while ( QPtr>=1 && QPtr<=(QSize-5) ){

	  /* Grab the next pixel in the queue and process it.*/
          curr = PixelQ[QPtr][1];
          curc = PixelQ[QPtr][2];
          QPtr--;
          Detection->Image[curr][curc] = NDetections+1;

  	  /* Add 4-connected neighbors to the queue.*/
          for (r=(curr-1); r<=(curr+1); r++){
            for (c=(curc-1); c<=(curc+1); c++){
              if (r==curr || c==curc){
                if (r>=1 && r<=Rows && c>=1 && c<=Cols){
                  if (Detection->Image[r][c] == 1){
                    QPtr++;
                    PixelQ[QPtr][1] = r;
                    PixelQ[QPtr][2] = c; 
    	            Detection->Image[r][c] = NDetections+1;
                  }            
                }
              }
            }
          }
        }

        if ( QPtr > (QSize-5) ){
          printf("WARNING - Queue for Region Growing Too Small.\n");
          printf("          A Detection Object Has Been Split.\n");
        }

      }
    }
  }
  free_imatrix(PixelQ,1,QSize,1,2);

  printf("\nImage Level Results...\n");
  printf("%d Detection(s) present\n",NDetections);


  if (NTargets == 0){

    TPs = 0;
    FPs = NDetections;
    DetectionSize = ivector(1,NDetections);
    for (i=1; i<=Rows; i++){
      for (j=1; j<=Cols; j++){
        if (Detection->Image[i][j] > 1){
          d = Detection->Image[i][j] - 1;
          DetectionSize[d]++;
        }
      }
    }
    for (d=1; d<=NDetections; d++)
      printf("FP detection: # pixels = %d\n",DetectionSize[d]);
    free_ivector(DetectionSize,1,NDetections);

  }else if (NDetections == 0){

    TPs = 0;
    FPs = 0;

  }else{

    TargetStatus    = ivector(1,NTargets);
    DetectionStatus = ivector(1,NDetections);
    TargetSize      = ivector(1,NTargets);
    DetectionSize   = ivector(1,NDetections);

    if (MetricType == OVERLAP){
      printf("Using overlap threshold = %5.2f\n",OvThresh);
      /* Count which detection pixels overlap which target pixels. */
      PixelOverlap  = imatrix(1,NTargets,1,NDetections);
      for (i=1; i<=Rows; i++){
        for (j=1; j<=Cols; j++){
      
          if (Truth->Image[i][j] > 1){
            t = Truth->Image[i][j] - 1;
            TargetSize[t]++;
          }else
            t = 0;

          if (Detection->Image[i][j] > 1){
            d = Detection->Image[i][j] - 1;
            DetectionSize[d]++;
          }else
            d = 0;

          if (t>0 && d>0){
            PixelOverlap[t][d]++;
          }

        }
      }

      for (t=1; t<=NTargets; t++){

        printf("Target %d: # pixels = %d\n",t,TargetSize[t]);
        for (d=1; d<=NDetections; d++){
          if (PixelOverlap[t][d] > 0){

            Intersection   = PixelOverlap[t][d];
            Union          = TargetSize[t] + DetectionSize[d] - Intersection;
            OverlapMeasure = (float)Intersection / (float)Union;

            if (OverlapMeasure > 0.0){
              printf("  Detection %d overlaps with %f...",d,OverlapMeasure);
            }
            if (OverlapMeasure > OvThresh){
              printf("Detected\n");
              DetectionStatus[d] = 1;
              TargetStatus[t]    = 1;
            }else printf("\n");
    
          }
        }

      }
      free_imatrix(PixelOverlap,1,NTargets,1,NDetections);

    }else if (MetricType == LOCATION){

      RTot     = vector(1,NDetections);
      CTot     = vector(1,NDetections);
      Centroid = imatrix(1,NDetections,1,2);
      for (i=1; i<=Rows; i++){
        for (j=1; j<=Cols; j++){
      
          if (Truth->Image[i][j] > 1){
            t = Truth->Image[i][j] - 1;
            TargetSize[t]++;
          }

          if (Detection->Image[i][j] > 1){
            d = Detection->Image[i][j] - 1;
            DetectionSize[d]++;
            RTot[d] += (float)i;
            CTot[d] += (float)j;
          }

        }
      }
      for (d=1; d<=NDetections; d++){
        Centroid[d][1] = (int)(RTot[d] / (float)DetectionSize[d] + 0.5);
        Centroid[d][2] = (int)(CTot[d] / (float)DetectionSize[d] + 0.5);
      }


      for (d=1; d<=NDetections; d++){
        r = Centroid[d][1];
        c = Centroid[d][2];
        if (Truth->Image[r][c] > 1){
          printf("TP detection: # pixels = %d\n",DetectionSize[d]);
          t = Truth->Image[r][c] - 1;
          DetectionStatus[d] = 1;
          TargetStatus[t]    = 1;
        }else{
          printf("FP detection: # pixels = %d\n",DetectionSize[d]);
        }
      }
      for (t=1; t<=NTargets; t++){
        printf("Target %d: # pixels = %d\n",t,TargetSize[t]);
        if (TargetStatus[t] == 1)
          printf("  Detected\n");
      }

      free_vector(RTot,1,NDetections);
      free_vector(CTot,1,NDetections);
      free_imatrix(Centroid,1,NDetections,1,2);

    }

    TPs = 0;
    FPs = 0;
    for (t=1; t<=NTargets; t++){
      if (TargetStatus[t] == 1) TPs++;
    }
    for (d=1; d<=NDetections; d++){
      if (DetectionStatus[d] == 0) FPs++;
    }

    free_ivector(TargetSize,1,NTargets);
    free_ivector(DetectionSize,1,NDetections);
    free_ivector(TargetStatus,1,NTargets);
    free_ivector(DetectionStatus,1,NDetections);

  }

  /* Restore truth template to original values.	*/  
  for (i=1; i<=Rows; i++){
    for (j=1; j<=Cols; j++){
      if (Truth->Image[i][j] > 1)
        Truth->Image[i][j] = 1;
      if (Detection->Image[i][j] > 1)
        Detection->Image[i][j] = 1;
    }
  }
  

}else{
  printf("ERROR: Truth image and detection template are different sizes\n");
}

printf("\nDETECTION SUMMARY:\n");
printf("  TPs = %d\n",TPs);
printf("  FPs = %d\n",FPs);

printf("-----------------------------------------\n");

/* Dump a summary of results to an output file. Append them so	*/
/* we can acummulate results for an entire set of images.	*/
if ((outfile = fopen(ResFilename,"a")) == NULL){
  printf("\nERROR: opening result file - %s\n", ResFilename);
  exit(99);
}

fprintf(outfile,"%d  %d  %d\n",NTargets, TPs, FPs);

fclose(outfile);

}/*Compare_TemplateImages*/

/************************************************************************/
/* Create a copy of an image.						*/
/************************************************************************/
TEMPLATE_IMAGE	*Copy_Template(ImTemplate)
TEMPLATE_IMAGE	*ImTemplate;
{
register  	i, j, Rows, Cols;
TEMPLATE_IMAGE	*ImTemplateCopy;

Rows           = ImTemplate->Rows;
Cols           = ImTemplate->Cols;
ImTemplateCopy = AllocateTemplate(Rows, Cols);
for(i=1; i<=Rows; i++){
  for(j=1; j<=Cols; j++){
    ImTemplateCopy->Image[i][j] = ImTemplate->Image[i][j];
  }
}
return(ImTemplateCopy);

}/*Copy_Template*/

/************************************************************************/
/* Scale a template image from one resolution to a coarser resolution. 	*/
/* Image resolution is expressed in microns per pixel. Original image	*/
/* remains unchanged. This procedure does not interpolate grey levels.	*/
/************************************************************************/
TEMPLATE_IMAGE	*Scale_Template(Im, FromRes, ToRes)
TEMPLATE_IMAGE	*Im;
float		FromRes, ToRes;
{
TEMPLATE_IMAGE	*ImScaled;
int		Rows, Cols, NewRows, NewCols;
register	i, j, x , y, b;
int		StartRow, StopRow, StartCol, StopCol, NewPixelValue;
float		NRowMicrons, NColMicrons, NewPixelSize;
float		Start_RowMicron, Stop_RowMicron, Start_ColMicron, Stop_ColMicron;
float		PixelRowMicrons, PixelColMicrons;
float		PixelStartMicron, PixelStopMicron;
float		*GreyLevelHistogram, MaxWeight, PixelWeight;

if (ToRes <= FromRes){

  if (ToRes < FromRes)
    printf("WARNING: Can only scale to coarser resolution...Nothing Done\n");
  return( Copy_Template(Im) );

}else{

  /* Compute the scaled image dimensions */
  Rows          = Im->Rows;
  Cols          = Im->Cols;
  NewPixelSize  = ToRes * ToRes;         /* New pixel size in square microns.	*/
  NRowMicrons   = FromRes * (float)Rows; /* Original image height in microns.	*/
  NColMicrons   = FromRes * (float)Cols; /* Original image width in microns.	*/
  NewRows       = (int)( NRowMicrons / ToRes + 0.5 ); /* # rows in scaled image.*/
  NewCols       = (int)( NColMicrons / ToRes + 0.5 ); /* # cols in scaled image.*/

  GreyLevelHistogram = vector(0,255);

  /* Allocate space for the scaled image. */
  ImScaled = AllocateTemplate(NewRows, NewCols);

  /* For each new row in the scaled image... */
  for (i=1; i<=NewRows; i++){

    /* Determine which pixels in the original image are associated with the 	*/
    /* pixel at Row i and Column j in the scaled image. 			*/ 
    Stop_RowMicron  = (ToRes * (float)i);
    Start_RowMicron = Stop_RowMicron - ToRes + 1.0;
    if (Stop_RowMicron > NRowMicrons) Stop_RowMicron = NRowMicrons;

    StartRow = (int)ceil( (double)(Start_RowMicron / FromRes) );
    StopRow  = (int)ceil( (double)(Stop_RowMicron / FromRes) );

    /* For each new column in the scaled image... */
    for (j=1; j<=NewCols; j++){

      Stop_ColMicron  = (ToRes * (float)j);
      Start_ColMicron = Stop_ColMicron - ToRes + 1.0;
      if (Stop_ColMicron > NColMicrons) Stop_ColMicron = NColMicrons;
      StartCol = (int)ceil( (double)(Start_ColMicron / FromRes) );
      StopCol  = (int)ceil( (double)(Stop_ColMicron / FromRes) );

      for (b=0; b<=255; b++) GreyLevelHistogram[b] = 0.0;
      for (x=StartRow; x<=StopRow; x++){

        PixelStopMicron  = FromRes * (float)x;
        PixelStartMicron = PixelStopMicron - FromRes + 1.0;
        if (PixelStartMicron < Start_RowMicron) PixelStartMicron = Start_RowMicron;
        if (PixelStopMicron > Stop_RowMicron)   PixelStopMicron = Stop_RowMicron;
        PixelRowMicrons = PixelStopMicron - PixelStartMicron + 1;

        for (y=StartCol; y<=StopCol; y++){

          PixelStopMicron  = FromRes * (float)y;
          PixelStartMicron = PixelStopMicron - FromRes + 1.0;
          if (PixelStartMicron < Start_ColMicron) PixelStartMicron = Start_ColMicron;
          if (PixelStopMicron > Stop_ColMicron)   PixelStopMicron = Stop_ColMicron;
          PixelColMicrons = PixelStopMicron - PixelStartMicron + 1;

          PixelWeight  = ((PixelRowMicrons * PixelColMicrons) / NewPixelSize);
          GreyLevelHistogram[ Im->Image[x][y] ] += PixelWeight;
        }
      }
      MaxWeight = 0.0;
      for (b=0; b<=255; b++){
        if (GreyLevelHistogram[b] > MaxWeight){
          MaxWeight     = GreyLevelHistogram[b];
          NewPixelValue = b;
        }
      }
      ImScaled->Image[i][j] = (unsigned char)NewPixelValue; 

    }
  }

  return(ImScaled);

}

}/*Scale_Template*/
